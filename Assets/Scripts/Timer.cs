﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Timer : MonoBehaviour

{
    public float timerz;
    private float timeLimit = 0f;

    public Text timerCountdownText;

    public GameObject[] women;

    void Update()
    {
        // TIMER STUFF

        // count the timerz float up in real time
        timerz -= Time.deltaTime;

        if (timeLimit >= timerz)
        {
            foreach (var woman in women)
            {
                if (woman.gameObject.tag == "Yellow")
                {
                    if (woman.GetComponent<SafetyChecker>().isSafe)
                    {
                        Stars.safeWomen++;
                    }
                }
                else if (woman.gameObject.tag == "Pink")
                {
                    Debug.Log(woman);
                    Debug.Log(woman.GetComponent<UncomfortableWomanChat>());
                    if (woman.GetComponent<UncomfortableWomanChat>().isSafe)
                    {
                        Stars.safeWomen++;
                    }
                }
            }

            LoadGameOver();
        }

        // TEXT DISPLAY STUFF
        timerCountdownText.text = Mathf.Round(timerz).ToString();


    }

    void LoadGameOver ()
    {
        Application.LoadLevel(2);
    }
}
