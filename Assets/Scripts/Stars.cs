﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Stars : MonoBehaviour
{
    public static int safeWomen = 0;
    public int safeWomenGoal;

    public Text starResult;

    void Update()
    {
        if (safeWomen > safeWomenGoal)
        {
            safeWomen = safeWomenGoal;
        }

        starResult.text = safeWomen.ToString() + " out of " + safeWomenGoal.ToString() +  " people protected!";
    }
}
