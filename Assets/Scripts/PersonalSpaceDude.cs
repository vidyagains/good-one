﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PersonalSpaceDude : MonoBehaviour
{

    public bool isCreepy = true;
    GameObject emoji;

    void Awake()
    {
        emoji = GetComponentInChildren<Emoji>().gameObject;
        emoji.SetActive(false);
    }

    void Update() {
        if (isCreepy)
        {
            emoji.SetActive(true);
        }
        else
        {
            emoji.SetActive(false);
        }
    }
}
