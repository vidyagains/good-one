﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RestartGame : MonoBehaviour
{
    public void LoadFirstLevel()
    {
        Application.LoadLevel(0);
    }

    public void LoadGameLevel()
    {
        Application.LoadLevel(1);
    }
}
