﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bob : MonoBehaviour
{
  public float xSpeed, ySpeed, zSpeed;
  public float xBob, yBob, zBob;
  public float xOffset, yOffset, zOffset;
  private Vector3 ogPos;
  public bool floorVal = false;
  public int floorInt;
  public bool absVal = false;

    void Start () {
      ogPos = transform.localPosition;
    }

    // Update is called once per frame
    void Update()
    {
        Vector3 nPos = ogPos;
        nPos.x = Mathf.Sin(xSpeed * (Time.time + xOffset)) * xBob;
        nPos.y = Mathf.Sin(ySpeed * (Time.time + yOffset)) * yBob;
        nPos.z = Mathf.Sin(zSpeed * (Time.time + zOffset)) * zBob;
        if (floorVal) {
          nPos.x = xBob * (Mathf.Floor(floorInt * (nPos.x))) / floorInt;
          nPos.y = yBob * (Mathf.Floor(floorInt * (nPos.y))) / floorInt;
          nPos.z = zBob * (Mathf.Floor(floorInt * (nPos.z))) / floorInt;
        }
        if (absVal) {
          nPos.x = Mathf.Abs (nPos.x) * (Mathf.Sign (xBob));
          nPos.y = Mathf.Abs (nPos.y) * (Mathf.Sign (yBob));
          nPos.z = Mathf.Abs (nPos.z) * (Mathf.Sign (zBob));
        }
        transform.localPosition = ogPos + nPos;
    }
}
