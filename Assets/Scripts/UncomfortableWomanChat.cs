﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UncomfortableWomanChat : MonoBehaviour
{
    public bool isSafe = false;

    private GameObject emoji;

    void Awake()
    {
        emoji = GetComponentInChildren<Emoji>().gameObject;
        emoji.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        if (!isSafe)
        {
            emoji.SetActive(true);
        }
        else
        {
            emoji.SetActive(false);
        }
    }


    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.tag == "Bro") {
            isSafe = false;
            //Stars.safeWomen -= 1;
            other.gameObject.GetComponent<PersonalSpaceDude>().isCreepy = true;
        }

    }

    private void OnTriggerExit2D(Collider2D other)
    {
        isSafe = true;
        //Stars.safeWomen += 1;

        if (other.gameObject.GetComponent<PersonalSpaceDude>())
        {
            other.gameObject.GetComponent<PersonalSpaceDude>().isCreepy = false;
        }
    }
}
