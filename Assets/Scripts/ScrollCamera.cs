﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScrollCamera : MonoBehaviour
{

    public float scrollSpeed = 10f;

    public GameObject endRight;
    public GameObject endLeft;
    float wheelInput;

    bool canMoveLeft;
    bool canMoveRight;
    Vector3 originalPosition;

    void Update()
    {

        wheelInput = Input.GetAxis("Mouse ScrollWheel");
    }

    void FixedUpdate()
    {
        if (Input.GetAxis("Mouse ScrollWheel") != 0)
        {
            if (transform.position.x > endRight.transform.position.x)
            {

                canMoveRight = false;
                canMoveLeft = true;
            }
            else if (transform.position.x < endLeft.transform.position.x)
            {
                canMoveLeft = false;
                canMoveRight = true;
            }
            else
            {
                canMoveLeft = true;
                canMoveRight = true;
            }

            if (canMoveLeft && canMoveRight)
            {
                transform.position += scrollSpeed * new Vector3(-Input.GetAxis("Mouse ScrollWheel"), 0, 0);
            }
            else if (wheelInput < 0 && canMoveRight == true)
            {
                transform.position += scrollSpeed * new Vector3(-Input.GetAxis("Mouse ScrollWheel"), 0, 0);
            }
            else if (wheelInput > 0 && canMoveLeft == true)
            {
                transform.position += scrollSpeed * new Vector3(-Input.GetAxis("Mouse ScrollWheel"), 0, 0);
            }
        }
    }
}
