﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EyelineChecker : MonoBehaviour
{

    public GameObject target;

    bool isStaring = true;
    GameObject emoji;

    void Awake()
    {
        emoji = GetComponentInChildren<Emoji>().gameObject;
        emoji.SetActive(false);
    }

    void FixedUpdate() {
        // Bit shift the index of the layer (8) to get a bit mask
        int layerMask = 1 << 8;

        // This would cast rays only against colliders in layer 8.
        // But instead we want to collide against everything except layer 8. The ~ operator does this, it inverts a bitmask.
        layerMask = ~layerMask;

        Vector3 direction = target.transform.position - transform.position;
        RaycastHit2D hit = Physics2D.Raycast(transform.position, direction, Mathf.Infinity, layerMask);

            if (hit.collider != null)
            {
                Debug.DrawRay(transform.position, direction, Color.red);

                if (hit.collider.gameObject == target)
                {
                    isStaring = true;
                    emoji.SetActive(true);
                    target.GetComponent<SafetyChecker>().isSafe = false;
                }
                else
                {
                    emoji.SetActive(false);
                    target.GetComponent<SafetyChecker>().isSafe = true;
                }
            }
    }
}