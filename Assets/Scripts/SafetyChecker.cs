﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SafetyChecker : MonoBehaviour
{
    public bool isSafe;

    GameObject emoji;

    void Awake() {
        emoji = GetComponentInChildren<Emoji>().gameObject;
        emoji.SetActive(false);
    }

    void Update() {
        if (!isSafe) {
            emoji.SetActive(true);
        }
        else {
            emoji.SetActive(false);
        }
    }
}
