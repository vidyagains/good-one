﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameState 
{
    #region Singleton
    private static GameState _instance = null;

    public static GameState Instance
    {
        get
        {
            if (_instance == null)
            {
                _instance = new GameState();
            }

            return _instance;
        }
    }
    #endregion

    public int SomeMemory = 0;

    public void SomeProgressCondition()
    {
        SomeMemory++;

        this.ChangeScene();
    }


    private void ChangeScene()
    {
       
    }
}
